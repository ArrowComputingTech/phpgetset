<?php
  
  class Car {
    public $name = "";
    function getName() : string {
      return $this->name;
    }
    function setName($name) {
      $this->name = $name;
    }
  }

  $car1 = new Car();
  $car1->setName("Olds");
  echo "Car 1 is a(n): " . $car1->getName() . "<br>";
  $car2 = new Car();
  $car2->setName("Chevy");
  echo "Car 2 is a(n): " . $car2->getName() . "<br>";
?>

